#/bin/bash
#
# AUTHOR: Dr. Daniel Balague Guardia
# Date: 12/30/19 
# 
# This script downloads and sets up
# Sage 8.9 to be used for homework
# assignments.

docker pull dbalague/sagemath:8.9

mkdir -p $HOME/Documents/homework_224
