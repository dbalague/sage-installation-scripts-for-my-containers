#/bin/bash
#
# Author: Daniel Balague Guardia
# Date: 12/30/2019
#
# This script starts Sage from a container 
# with a specific directory.
#
#################################################
# Arguments: 
#   (optional) Path to the homework.
#   If not specified, the default is used.
#################################################
# Returns:
#   none
#################################################

if [[ $1 == "" ]]; then
    docker run -v $HOME/Documents/homework_224:/home/sage/MATH224 -ti -p8888:8888  --name sage dbalague/sagemath:8.9 sage-jupyter
else
    docker run -v $1:/home/sage/MATH224 -ti -p8888:8888 --name sage dbalague/sagemath:8.9 sage-jupyer
fi

echo "Deleting Sage container..."
docker rm sage
echo "Container deleted!"
