##############################################################
#
#  Warning: I am not a PowerShell expert programmer.
#           By using this script I am no longer responsible
#           for any possible damage that may occur to your
#           computer. Use under your responsibility.
#
#
# Author: Dr. Daniel Balague Guardia
# Date: 01/10/2020
#
#
##############################################################

function show_warning
{

$warning_string=@'
###############################################
WARNING
-------
I am not a PowerShell expert programmer.
By using this script I am no longer responsible
for any possible damage that may occur to your 
computer. Use under your responsibility.
###############################################
'@

    Write-Host $warning_string
}

##############################################################
# This function prompts the user for a yes or no answer
# Inputs: None
# Outputs: the letters 'y' or 'n'
##############################################################

function yes_or_no
{ 

    while( ($answer -ne "y") -and ($answer -ne "n") ){

        $answer = Read-Host -Prompt 'Proceed? (y/n)'

    }

    return $answer
}

show_warning

if( $args.count -lt 1 ){
    Write-Host "Starting Sage..."
    docker run -v "$HOME/Documents/homework_224:/home/sage/MATH224" -ti -p8888:8888  --name sage dbalague/sagemath:8.9 sage-jupyter
}
else{
    Set-Variable -Name DIR_NAME -Value $args[0]
    Set-Variable -Name HOMEWORK_PATH -Value "$HOME/Documents/$DIR_NAME"
    Write-Host "We will use $HOMEWORK_PATH as the homework directory."
    $answer = yes_or_no
    if( $answer -eq "y" ){
         Write-Host "Starting Sage..."
         docker run -v "${HOMEWORK_PATH}:/home/sage/MATH224" -ti -p8888:8888 --name sage dbalague/sagemath:8.9 sage-jupyter
    }else{
        Write-Host "Operation Cancelled"
        Exit-PSSession
    }
}

Write-Host "Deleting Sage Container..."
docker container rm sage
Write-Host "Done"
