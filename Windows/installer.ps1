##############################################################
#
#  Warning: I am not a PowerShell expert programmer.
#           By using this script I am no longer responsible
#           for any possible damage that may occur to your
#           computer. Use under your responsibility.
#
#
# Author: Dr. Daniel Balague Guardia
# Date: 01/10/2020
#
#
##############################################################

##############################################################
# This script downloads and sets up
# Sage 8.9 to be used for homework
# assignments.
##############################################################

function show_warning
{

$warning_string=@'
###############################################
WARNING
-------
I am not a PowerShell expert programmer.
By using this script I am no longer responsible
for any possible damage that may occur to your 
computer. Use under your responsibility.
###############################################
'@

    Write-Host $warning_string
}

##############################################################
# This function prompts the user for a yes or no answer
# Inputs: None
# Outputs: the letters 'y' or 'n'
##############################################################

function yes_or_no
{ 

    while( ($answer -ne "y") -and ($answer -ne "n") ){

        $answer = Read-Host -Prompt 'Proceed? (y/n)'

    }

    return $answer
}

show_warning

$install_sage = yes_or_no
if( $install_sage -eq "y" ){

    Write-Host "Downloading Sage..."
    docker pull dbalague/sagemath:8.9

    Write-Host "Creating Homework Directory in your Documents"
    $HOMEWORK_DIR_PATH = "$HOME/Documents/homework_224"

    if( Test-Path $HOMEWORK_DIR_PATH ){

        Write-Host "Homework Directory $HOMEWORK_DIR_PATH Already Exists"
        Exit-PSSession

    }else{

        mkdir -p $HOMEWORK_DIR_PATH

    }
}
