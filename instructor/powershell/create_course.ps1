##############################################################
#
#  Warning: I am not a PowerShell expert programmer.
#           By using this script I am no longer responsible
#           for any possible damage that may occur to your
#           computer. Use under your responsibility.
#
#
# Author: Dr. Daniel Balague Guardia
# Date: 01/04/2020
#
#
##############################################################

function show_warning
{

$warning_string=@'
###############################################
WARNING
-------
I am not a PowerShell expert programmer.
By using this script I am no longer responsible
for any possible damage that may occur to your 
computer. Use under your responsibility.
###############################################
'@

    Write-Host $warning_string
}

##############################################################
# This function creates the nbgrader_config.py file
# form the course_config.py
#
# Inputs: course, course path
# Outputs: nbgrader_config.py file in users directory
##############################################################

function create_config
{
    Param
    (
         [Parameter(Mandatory=$true, Position=0)]
         [string] $arg1,
         [Parameter(Mandatory=$true, Position=1)]
         [string] $arg2
    )
    cat ../course_config.py | %{$_ -replace "MATH224",$arg1 } > $arg2/nbgrader_config.py
}

##############################################################
# This function prompts the user for a yes or no answer
# Inputs: None
# Outputs: the letters 'y' or 'n'
##############################################################

function yes_or_no
{ 

    while( ($answer -ne "y") -and ($answer -ne "n") ){

        $answer = Read-Host -Prompt 'Proceed? (y/n)'

    }

    return $answer
}


##############################################################
# MAIN SCRIPT
##############################################################

show_warning

Write-Host "Creating Course Structure"

if ( $args.count -lt 1 ){

    Write-Host "You did not intrduce any parameters"

}else{

    Write-Host "According to your input, the course information is the following:"

    Write-Host "Your Course Name is" $args[0]
    Set-Variable -Name COURSE_NAME -Value $args[0]
    Set-Variable -Name COURSE -Value "$HOME/Documents/$COURSE_NAME"
    Write-Host "We will create the Course in the directory " $COURSE

    $create_course = yes_or_no

    if( $create_course -eq "y" ){

        Write-Host "Creating the Course directory in" $COURSE

        if( Test-Path $COURSE ){

            Write-Host "Course Already Exists"
            Exit-PSSession

        }
        else{

            Write-Host "Creating $COURSE"
            New-Item -ItemType directory -Path $COURSE
            Write-Host "Creating $COURSE/exchange"
            New-Item -ItemType directory -Path $COURSE/exchange
            Write-Host "Creating $COURSE/source"
            New-Item -ItemType directory -Path $COURSE/source
            Write-Host "Creating $SOURCE/submitted"
            New-Item -ItemType directory -Path $COURSE/submitted

        }

        create_config $COURSE_NAME $COURSE

        if( $args.count -gt 1){

            Write-Host "The number of assignments is" $args[1] "(it can be modified later)."
            Write-Host $args[1] " directories will be created in your course."

            $create_assignment_dirs = yes_or_no

            if( $create_assignment_dirs -eq "y" ){

                for( $i = 1; $i -le $args[1] ; $i++ ){

                    $HOMEWORK_DIR=('homework{0:d2}' -f $i)
                    $HOMEWORK_DIR_PATH="$COURSE/source/$HOMEWORK_DIR"

                    if( Test-Path $HOMEWORK_DIR_PATH ){

                        Write-Host "Assingment $HOMEWORK_DIR_PATH Already Exists"
                        Exit-PSSession

                    }else{

                        Write-Host 'Creating direcory' $HOMEWORK_DIR_PATH
                        New-Item -ItemType directory -Path $HOMEWORK_DIR_PATH

                    }
                }
            }
            else{

                Write-Host "Operation cancelled"
                Write-Host "DONE! Course created!"
                Write-Host "Your course can be found in " $COURSE
                Exit-PSSession

            }
        }
    }
    else{

        Write-Host "Operation cancelled"
        Exit-PSSession

    }
}

Write-Host "DONE! Course created!"
