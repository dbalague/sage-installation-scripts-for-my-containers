#/bin/bash
#
################################################################### 
# This Script sets up the course in a 
# Unix/Linux environment
#
# Author: Dr. Daniel Balague Guardia
# Date: 01/06/2020
################################################################### 

answer=""


################################################################### 
#
# This function prompts the user for a yes/no answer
#
# INPUTS: none
#
# OUTPUTS: 1 if yes 0 if no
# 
################################################################### 

function yes_or_no {
    
    while [ "$answer" != "y" ] && [ "$answer" != "n" ]; do
        read -p 'Proceed? (y/n): ' answer
    done

    if [ $answer = "y" ]; then
        return 1
    else
        return 0
    fi
}

################################################################### 
#
# This function creates the course configuration
#
# INPUTS: course name
#
# OUTPUTS: a file with the nbgrader configuration
# 
################################################################### 

function create_config() {

    sed "s/MATH224/$1/g" ../course_config.py > $2/nbgrader_config.py

}


################################################################### 
# Main loop
################################################################### 
NUM_ARGS=$#

if [ $NUM_ARGS -lt 1 ]; then
    echo "You did not enter any input"
else
    COURSE=$1
    COURSE_PATH=$HOME/Documents/$1
    echo "Creating Course Structure"
    echo
    echo "According to your input, the course information is the following:"
    echo "Your Course Name is $COURSE"
    echo

    echo "We will create the Course your in the directory $COURSE_PATH"
    yes_or_no
    ANSWER=$?
    answer=""

    if [ $ANSWER -eq 0 ]; then
        echo "Operation Cancelled!"
        echo
        exit
    else
        if [ -d $COURSE_PATH ]; then
            echo "Directory already exists!"
            echo
        else
            mkdir -p $COURSE_PATH/{"exchange","source","submitted"}
            echo "Directory Created"
            echo
        fi

        create_config $COURSE $COURSE_PATH

        if [ $NUM_ARGS -gt 1 ]; then
            echo "The number of assingments is $2 (it can be modified later)."
            echo "$2 Directories will be created."

            yes_or_no
            ANSWER=$?
            answer=""

            if [ $ANSWER -eq 0 ]; then
                echo "Operation Cancelled!"
                echo "Your course can be found in $COURSE_PATH"
                exit
            else
                for i in $(seq 1 1 $2); do
                    ASSIGNMENT=$(printf "homework%02d" $i)
                    HOMEWORK_PATH="$COURSE_PATH/source/$ASSIGNMENT"
                    if [ -d $HOMEWORK_PATH ]; then
                        echo "Directory $HOMEWORK_PATH already exists"
                    else
                        echo "Creating $HOMEWORK_PATH"
                        mkdir -p $HOMEWORK_PATH
                    fi
                done
            fi
        fi
        echo "Your course can be found in $COURSE_PATH"
    fi
fi

