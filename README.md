# Sage Installation Scripts For My Containers

In order to use these scripts, one needs to have Docker installed and running.

## WARNING
By using these scripts you agree that I will not be responsible for any damage
occured to your computer after using these scripts.

## Introduction

The scripts in this repo help you set up the environment for running SageMath.
There are two versions of the scripts:

- Unix (Mac)/Linux
- Windows

## Installing Sage

Follow these instructions for installing Sage:

- For Unix/Linux:
	
	1. Download the corresponding version of the scripts for Unix.

	2. Open a Terminal in the directory where you downloaded the files.
	
	3. Give execution permission to the scripts:
	   `chmod u+x *.sh`
	4. Run the script `installer.sh`. A new directory called `homework_224`
		will be created in your `$HOME/Documents` directory. This is where you will put your homework.
		
- For Windows: 
	1. Right-click on the `installer.ps1` script and click on "Run with PowerShell". A new directory called `homework_224`
		will be created in your `$HOME/Documents` directory. This is where you will put your homework.
	
## Running Sage

- If you are a student:
	- For Windows:
		- Right-click the script `SageMath.ps1` and click on "Run with PowerShell".
	- For Unix/Linux:
		- To use SageMath, run the script `SageMath.sh`.
  	- To log into the Jupyter Notebook interface, open a browser and type `localhost:8888` and copy-paste
		the token provided. 
		
- If you are an Instructor:
	- Run the script `SageMath_Instructor.sh` following the instructions in the "Instructor's Manual".